# Subiectul 3: REST

## Se da urmatorul WEB API, implementat in fisierul server.js ce are implementate metode HTTP de tip POST si GET pentru adaugarea de produse si obtinerea tuturor produselor.
## Sa se implementeze metode de tip PUT si DELETE pentru update-ul de produse in functie de id si stergerea de produse in functie de denumire.
## (Note: pentru PUT se va transmite `id-ul` ca si path param iar pentru DELETE se va transmite numele produsului ca si payload in body);