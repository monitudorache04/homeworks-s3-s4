const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

//PUT
app.put("/update/product/:id", (req,res) =>{
    
    const id = req.params.id;
    let found = false;
    
    
    products.forEach((product)=>{
        
        if(product.id == id){
            found = true;
            
            product.productName = req.body.productName;
            product.price = req.body.price;
        }
    })
    
    if(found){
        res.status(200).send(`The product with id ${id} was updated!`);
    }
    else{
        res.status(404).send(`Could not find resource with id ${id}!`)
    }
    
})



//DELETE
app.delete("/delete", (req,res)=>{
    
    
    const nameProductToBeDeleted = req.body.productName;
    var position = -1;
    
    for(var i=0; i<products.length;i++){
        
       var product = products[i];
       if(product.productName == nameProductToBeDeleted){
           position = i;
           break;
       }
       
    }
    
    
     products.splice(i,1);
     
     if(position == -1){
         res.status(404).send("Product could not be found!");
     }
     else{
         res.status(200).send("Product deleted!")
     }
    
    
})


app.listen(8080, () => {
    console.log('Server started on port 8080...');
});