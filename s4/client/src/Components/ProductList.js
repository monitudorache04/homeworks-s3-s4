import React, { Component } from 'react';


class ProductList extends Component {
    
    
    constructor(props){
        super(props);
    }
  
  

  render() {
     
      let items = this.props.source.map((product, index)=>{
          return <div key={index}>{product.productName}</div>
      })
    
    return (
      <div className="ProductList">
      
      <h1>{this.props.title}</h1>
      <div>
      {items}
      </div>
      
            
        
      </div>
    );
  }
}

export default ProductList;
