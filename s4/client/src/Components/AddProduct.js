import React, { Component } from 'react';
import axios from 'axios';


class AddProduct extends Component {
    
    constructor(props){
        super(props);
        
        this.state = {};
        
        this.state.productName = ""
        this.state.price = ""
    }
    
   
    
    handleChangeProductName = (event) => {
        this.setState({
            productName: event.target.value
        })
    }
    
    handleChangePrice = (event) => {
        this.setState({
            price: event.target.value
        })
    }
    
    
    handleAddClick = () => {
        let product = {
            productName: this.state.productName,
            price: this.state.price
        }
        
        
        axios.post("https://my-project-1078-monitudorache.c9users.io/add", product).then((response)=>{
            if(response.status === 2000){
                this.props.productToBeAdded(product)
            }
        }).catch((err)=>{
            console.log("It doesn t work!:(")
        })
        
    }

  render() {
      
    
    return (
      <div className="AddProduct">
      <h1>Add a product</h1>
      <input type="text" placeholder="Product name" 
             onChange={this.handleChangeProductName} 
             value={this.state.productName}/>
             
             
      <input type="number" placeholder="Price"
             onChange={this.handleChangePrice}
             value={this.state.price}/>
      
      
      <button onClick={this.handleAddClick}>Add product</button>
            
        
      </div>
    );
  }
}

export default AddProduct;
