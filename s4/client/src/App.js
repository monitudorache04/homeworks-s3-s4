import React, { Component } from 'react';

import './App.css';
import ProductList from "./Components/ProductList.js";
import AddProduct from "./Components/AddProduct.js";


class App extends Component {
  
  
  constructor(props){
    super(props);
    this.state = {};
    
    
    this.state.products = []; 
  }
  
  

  onProductAdded = (product) => {
    let products = this.state.products;
    
    products.push(product);
    products.setState({
      products:products
    })
  }

  
  componentWillMount(){
  
    const url = "https://my-project-1078-monitudorache.c9users.io/get-all"
    
    fetch(url).then((response) => {
      return response.json();
    }).then((products) => {
      this.setState({
        products:products
      })
    })
    
  }
  

  

  
  render() {
    
    return (
      <div className="App">
      
     
      
      
      <ProductList title="Product List" source={this.state.products}></ProductList>    
      <AddProduct productToBeAdded={this.onProductAdded}/>
      </div>
    );
  }
}

export default App;
